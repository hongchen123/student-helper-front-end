import React, { Fragment, useState, useEffect } from 'react';
// import PropTypes from 'prop-types';
import { Breadcrumbs } from 'shared/components';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@mui/material';
import { Header, BoardName } from '../Board/Header/Styles';
import ProjectCard from './components/ProjectCard';
import projectData from './components/ProjectData';

// const propTypes = {
//     project: PropTypes.object.isRequired,
//     fetchProject: PropTypes.func.isRequired,
//   };

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: '20px',
  },
  outergrid: {
    width: '100%',
    justifyContent: 'center',
    // margin: 'auto',
  },
}));

const Dashboard = () => {
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    fetch('http://localhost:2020/projects')
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        throw new Error('Failed to fetch data.');
      })
      .then(result => {
        setProjects(result);
      })
      .catch(error => {
        console.log(error.message);
        console.log('Back-end is offline');
        // throw new Error('Back-end is offline.');
      });
  }, []);

  console.log(projects);
  console.log(projectData);
  const classes = useStyles();
  const match = useRouteMatch();
  const history = useHistory();
  const finalObject = {};
  const i = 0;
  // projectData.map(data => (
  //   let temp =
  //   // i = i+1;
  // ))

  return (
    <Fragment>
      <Breadcrumbs items={['Projects', 'Dashboard']} />
      <Header>
        <BoardName>Capstone Projects</BoardName>
      </Header>
      <Grid container className={classes.container}>
        {projectData.length > 0 && (
          <div id="projects">
            <Grid container spacing={6} className={classes.outergrid}>
              {projectData.map(data => (
                <Grid item sm={12} md={6} lg={4} key={data.title}>
                  <ProjectCard dataItem={data} />
                </Grid>
              ))}
            </Grid>
          </div>
        )}
      </Grid>
    </Fragment>
  );
};

// Dashboard.propTypes = propTypes;

export default Dashboard;
