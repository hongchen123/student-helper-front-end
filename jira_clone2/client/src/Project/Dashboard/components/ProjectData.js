import zotSearch from '../assets/images/zotSearch.PNG';
import food from '../assets/images/food.PNG';
import stock from '../assets/images/stock.PNG';
import drive from '../assets/images/drive.PNG';
import sale from '../assets/images/sale.PNG';
import hike from '../assets/images/hike.PNG';
import rec from '../assets/images/recNet.PNG';
import flat from '../assets/images/flat.PNG';
import cartoon from '../assets/images/cartoon.PNG';
import phisher from '../assets/images/phisher.PNG';
import tap from '../assets/images/tap.PNG';
import chatbot from '../assets/images/chatbot.PNG';
import easy from '../assets/images/easy.PNG';
import restoration from '../assets/images/restoration.PNG';
import ev from '../assets/images/ev.PNG';
import bill from '../assets/images/bill.PNG';
import covid from '../assets/images/covid.PNG';
import insta from '../assets/images/insta.PNG';
import auction from '../assets/images/auction.PNG';
import collab from '../assets/images/collab.PNG';
import counter from '../assets/images/counter.PNG';
import theme from '../assets/images/theme.PNG';
import itinerary from '../assets/images/itinerary.PNG';


const ProjectData = [
    {
        image: zotSearch,
        id: 1,
        imgAlt: 'alt',
        title: 'In House Search Engine',
        video: 'jBVOIJN0XcY',
        description: ["A search engine that will work on the company's server and intelligently provide the user with search results along with specific highlights.",
        'The search would help increase the overall efficiency of a team as the time spend looking for information would be reduced.']
    },
    {
        image: hike,
        id: 2,
        imgAlt: 'alt',
        title: 'Smart Hiking',
        video: '_QL9iVuB9co',
        description: ["Hiking and Trail enthusiasts need a one-stop application to discover unexplored trails depending on their preferences and by taking into consideration all the safety gear that needs to be equipped before embarking on the journey.",
        'An application hopes to mitigate this issue by creating a web-app with ease of use and access for trails all over the country.']
    },
    {
        id: 1,
        image: rec,
        imgAlt: 'alt',
        title: 'RecNet',
        video: 'rub40jP1k1Q',
        description: ["Cross-platform content recommendation engine.",
        'Suggests content and friends based on previous web activity on YouTube, Spotify and/or Reddit.']
    },
    {
        id: 1,
        image: flat,
        imgAlt: 'alt',
        title: 'FlatMate Finder',
        video: 'L0B0rRp_uI0',
        description: ["Online platform which will help students find flatmate and apartments when they shift to a foreign University.",
        'Ability to filter out potential profiles based on their preferences and select the ideal candidates for the flatmates.']
    },
    {
        id: 1,
        image: cartoon,
        imgAlt: 'alt',
        title: 'Cartoonify',
        video: 'Va8Kw4JXAoQ',
        description: ["A web application which can convert images to cartoon version."]
    },
    {
        image: phisher,
        imgAlt: 'alt',
        title: 'The Phisher',
        video: 'FCL8ZxULkGk',
        description: ["Ability to uniquely identify and classify all of incoming emails as safe or malicious.",
        'Monitor any suspected emails to see if you are being targeted and allow you to react accordingly.']
    },
    {
        image: tap,
        imgAlt: 'alt',
        title: 'Tap News',
        video: 'nGUpgwNyKD0',
        description: ["Real-time news scraping and recommendation system."]
    },
    {
        image: chatbot,
        imgAlt: 'alt',
        title: 'Recommender ChatBot',
        video: null,
        description: ["A chatbot application that can understand user preferences and provide corresponding recommendations about restaurants or movie theaters."]
    },
    {
        image: easy,
        imgAlt: 'alt',
        title: 'EasyRental',
        video: 'lB7eFNo3TmU',
        description: ["A platform where users can put up items for others to borrow and can search for items to rent.",
        'An Airbnb of sorts for household items.']
    },
    {
        image: restoration,
        imgAlt: 'alt',
        title: 'Image Restoration WebApp',
        video: 'WQMblXg73gM',
        description: ["A web app that allows people to apply various transformations.",
        'Including Image Restoration, Style Transfer, Black & White to Colored Image.']
    },
    {
        image: ev,
        imgAlt: 'alt',
        title: 'EV Route Planner',
        video: null,
        description: ["A website to help EV users make a route plan to decide when and where to charge their vehicles based on vehicle's range."]
    },
    {
        image: bill,
        imgAlt: 'alt',
        title: 'BillShare',
        video: '19xptNLXG54',
        description: ["Application that helps people manage their bills better.",
        'Manage payment status of sharing billls for creators and debtors.',
        'Real-time communication between users.']
    },
    {
        image: covid,
        imgAlt: 'alt',
        title: 'Covid Exposure',
        video: null,
        description: ["Get exposed people and notify them according to visitor's record."]
    },
    {
        image: insta,
        imgAlt: 'alt',
        title: 'Instagram Giveaway',
        video: null,
        description: ["A web service that help influencers to host a giveaway on Instagram.",
        'This service will pick a random account by follwers or by posts, and this account will be the giveaway winner.']
    },
    {
        image: auction,
        imgAlt: 'alt',
        title: 'ZotVerse Auction Platform',
        video: 'Ndds8oUNzw4',
        description: ["Online bidding and marketplace platform for UCI students, alumni and faculty members.",
        'Auction off their items including furniture, tickets to concerts, notes for lectures and much more.']
    },
    {
        image: collab,
        imgAlt: 'alt',
        title: 'Collab',
        video: '2pCfQ1xXIX8',
        description: ["A collaborative study web platform for students.",
        'Ability to conduct group study and video sessions, real-time notes, screen sharing with peers, and track individual progress over time.']
    },
    {
        image: counter,
        imgAlt: 'alt',
        title: 'CounterFeit Product Detection using Blockchain',
        video: 'digEVWgolnw',
        description: ["An Anti Counterfeit System using Blockchain.",
        'Provides security to the clients by offering data to clients.']
    },
    {
        image: theme,
        imgAlt: 'alt',
        title: 'Theme Park Planner',
        video: 'usiPizdTeUI',
        description: ["A website that predicts an optimal path for theme park visitors."]
    },
    {
        image: itinerary,
        imgAlt: 'alt',
        title: 'Itinerary Planner',
        video: null,
        description: ["An itinerary planner mobile application to suggest tourist attractions at the destination location of the user's choice based on rating, the distance between tourist places, and time constraints."
        ]
    },
    {
        image: sale,
        imgAlt: 'alt',
        title: 'MoveOut Sale',
        video: 'WVhB0xeZUvA',
        description: ["A web service for buying and selling products for college students moving in/out of apartments.",
        'Validate users through their student IDs.']
    },
    {
        image: drive,
        imgAlt: 'alt',
        title: 'ZotDrive',
        video: 'OxqTAapIAFk',
        description: ["A cloud based file storage system with features of notes/tags per file.",
        'File synchronization across devices.']
    },
    {
        image: stock,
        imgAlt: 'alt',
        title: 'Stock Trading Advisor',
        video: null,
        description: ["Application that helps users build their portfolio by using the highly customizable stock filtering tool.",
        'Backtest tool which allows user to compare their portfolio with the market.']
    },
    {
        image: food,
        imgAlt: 'alt',
        title: 'One Stop Food Application',
        video: null,
        description: ["A unified food application based on food classification.",
        'Identify the food item from uploaded pictures and provide its recipe.',
        "Provide list of nearby restaurants that serve the food item."]
    }
]

export default ProjectData;