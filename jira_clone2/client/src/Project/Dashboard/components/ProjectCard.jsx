import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import { CardActionArea, Grid, Card, CardMedia, CardContent, Typography, Button } from '@mui/material';
import { Link } from 'react-router-dom';
import { Icon } from 'shared/components';
import YoutubeEmbed from "./Video";

// primary: '#0052cc', // Blue
//   success: '#0B875B', // green
//   danger: '#E13C3C', // red
//   warning: '#F89C1C', // orange
//   secondary: '#F4F5F7', // light grey

//   textDarkest: '#172b4d',
//   textDark: '#42526E',
//   textMedium: '#5E6C84',
//   textLight: '#8993a4',
//   textLink: '#0052cc',

//   backgroundDarkPrimary: '#0747A6',
//   backgroundMedium: '#dfe1e6',
//   backgroundLight: '#ebecf0',
//   backgroundLightest: '#F4F5F7',
//   backgroundLightPrimary: '#D2E5FE',
//   backgroundLightSuccess: '#E4FCEF',

//   borderLightest: '#dfe1e6',
//   borderLight: '#C1C7D0',
//   borderInputFocus: '#4c9aff',

const useStyles = makeStyles(theme => ({
    card: {
        // height: '100%',
        width: '95%',

    },
    videoSection: {
        width: "100%",
        height: "auto",
        border: '1px solid black'
    },
}))

const ProjectCard = ({dataItem}) => {
    const classes = useStyles();

    return(
        
            <Card className={classes.card} style={{backgroundColor:'#dfe1e6'}}>
                <CardActionArea >
                    {/* <Link to={{
                        pathname: "/user-pages/single-project",
                        state: {dataItem}
                    }}> */}
                        <CardMedia component='img'
                            alt={dataItem.imgAlt}
                            height='200'
                            width='100%'
                            image={dataItem.image}
                            title={dataItem.imgAlt}
                            />
                    {/* </Link>    */}
                </CardActionArea>
                <CardContent style={{backgroundColor:'#dfe1e6'}}>
                    <Typography gutterBottom style={{fontFamily:"CircularStdMedium", fontWeight:"normal", fontSize:24, textAlign:'center'}}>
                        {dataItem.title}
                    </Typography>
                    {dataItem.description.map(item => (
                        <Typography key={item} component="p" style={{fontFamily:"CircularStdBook", fontWeight:"normal", fontSize:'15', justify:'center', paddingTop:'10px', }}>
                            {item}
                        </Typography>
                    ))}
                </CardContent>
                <Link to={{
                        pathname: "/project/board",
                        state: {dataItem}
                    }}>

                    <Button variant="contained" startIcon={<Icon type='calendar'/>} style={{margin:"0 0 20px 0", width:"100%", backgroundColor:'#0747A6', fontFamily:"CircularStdBook"}}>
                        Jira Tracker
                    </Button>
                   
                </Link>
                {dataItem.video && <Grid container className={classes.videoSection}>
                    <YoutubeEmbed embedId={dataItem.video}/>
                </Grid>                
                 }
                

            </Card>
        


    )
}

export default ProjectCard;