import React, { useState, useEffect } from 'react';

import { Route, Redirect, useRouteMatch, useHistory } from 'react-router-dom';

import useApi from 'shared/hooks/api';
import { updateArrayItemById } from 'shared/utils/javascript';
import { createQueryParamModalHelpers } from 'shared/utils/queryParamModal';
import { PageLoader, PageError, Modal } from 'shared/components';

import NavbarLeft from './NavbarLeft';
import Sidebar from './Sidebar';
import Board from './Board';
import IssueSearch from './IssueSearch';
import IssueCreate from './IssueCreate';
import ProjectSettings from './ProjectSettings';
import { ProjectPage } from './Styles';
import Dashboard from './Dashboard';
// import test from './Dashboard/assets/images/auction.PNG';
// import { CardActionArea, Grid, Card, CardMedia, CardContent, Typography } from '@mui/material';
const axios = require('axios').default;

const Project =  () => {
  const match = useRouteMatch();
  const history = useHistory();


  const issueSearchModalHelpers = createQueryParamModalHelpers('issue-search');
  const issueCreateModalHelpers = createQueryParamModalHelpers('issue-create');
  const [project, setProject] = useState(null);

  const updateProject = (toUpdate) => {
    console.log(" To update", toUpdate)
    setProject(toUpdate)
  }
  !project && axios.get('http://localhost:2020/projects/1')
  .then(function (response) {
    // handle success
    console.log(response.data);
    setProject(response.data.project);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  });

  const [{ data, error, setLocalData }, fetchProject] = useApi.get('http://localhost:2020/projects/1');
  project && console.log('Test',project.users);
  
  // if (!data) return <PageLoader />;
  // if (error) return <PageError />;



// const updateLocalProjectIssues = (issueId, updatedFields) => {
//   setLocalData( (currentData = project)  => ({
//     project: {
//       ...currentData.project,
//       issues: updateArrayItemById(currentData.project.issues, issueId, updatedFields),
//     },
//   }));
// };

const updateLocalProjectIssues = (issueId, updatedFields) => {
  setLocalData( ()  => ({
    project: {
      ...project,
      issues: updateArrayItemById(project.issues, issueId, updatedFields),
    },
  }));
};
if(!project){
  return 'loading...'
}
  return (
    <ProjectPage>
      <NavbarLeft
        issueSearchModalOpen={issueSearchModalHelpers.open}
        issueCreateModalOpen={issueCreateModalHelpers.open}
      />

      <Sidebar project={project} />

      {issueSearchModalHelpers.isOpen() && (
        <Modal
          isOpen
          testid="modal:issue-search"
          variant="aside"
          width={600}
          onClose={issueSearchModalHelpers.close}
          renderContent={() => <IssueSearch project={project} />}
        />
      )}

      {issueCreateModalHelpers.isOpen() && (
        <Modal
          isOpen
          testid="modal:issue-create"
          width={800}
          withCloseIcon={false}
          onClose={issueCreateModalHelpers.close}
          renderContent={modal => (
            <IssueCreate
              project={project}
              fetchProject={fetchProject}
              onCreate={() => history.push(`${match.url}/board`)}
              modalClose={modal.close}
            />
          )}
        />
      )}

      <Route
        path={`${match.path}/dashboard`}
        render={() => (
          <Dashboard project={project} fetchProject={fetchProject}/> 

        )}
      />
              
      <Route
        path={`${match.path}/board`}
        render={() => (
          <Board
            project={project}
            setProject = {updateProject}
            fetchProject={fetchProject}
            updateLocalProjectIssues={updateLocalProjectIssues}
          />
        )}
      />

      <Route
        path={`${match.path}/settings`}
        render={() => <ProjectSettings project={project} fetchProject={fetchProject} />}
      />

      {match.isExact && <Redirect to={`${match.url}/board`} />}
    </ProjectPage>
  );
};

export default Project;
