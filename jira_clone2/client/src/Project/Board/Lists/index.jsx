import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext } from 'react-beautiful-dnd';

import useCurrentUser from 'shared/hooks/currentUser';
import api from 'shared/utils/api';
import toast from 'shared/utils/toast';
import { moveItemWithinArray, insertItemIntoArray } from 'shared/utils/javascript';
import { IssueStatus } from 'shared/constants/issues';

import List from './List';
import { Lists } from './Styles';
const axios = require('axios').default;

const propTypes = {
  project: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired,
  // updateLocalProjectIssues: PropTypes.func.isRequired,
  fetchProject: PropTypes.func.isRequired,
};

const ProjectBoardLists = ({ project, setProject, filters, fetchProject }) => {
  const { currentUserId } = useCurrentUser();

  const handleIssueDrop = async (args) => {
    console.log(args);
    const { draggableId, destination, source } = args;
    if (!isPositionChanged(source, destination)) return;

    const issueId = Number(draggableId);
    console.log("Inga irukane", issueId, destination, source);

      try {
        await axios.post(`http://localhost:2020/issues/update/${issueId}`,{
          status: destination.droppableId,
          // listPosition: 0
          listPosition: calculateIssueListPosition(project.issues, destination, source, issueId)
        }).then(() => {
          // setProject();
          console.log("project", project.issues[source['index']].status, " dest id", destination.droppableId)
          const projectCopy = structuredClone(project)
          console.log(project)
          const updatedProjectIssues = project.issues.map( (item) => {
            if (item.id === issueId && item.status === source.droppableId){
              console.log(item.title, 'changing this')
              item.status = destination.droppableId
              item.listPosition = calculateIssueListPosition(project.issues, destination, source, issueId)
            }
            return item
          });
          projectCopy.issues = updatedProjectIssues;
          console.log(projectCopy, 'updated issues', updatedProjectIssues);
          // projectCopy.issues[projectIndex].status = destination.droppableId
          // console.log("after change", projectCopy.issues[projectIndex])

          
          // project.issues[destination.draggableId] = project.issues[source['index']]
          console.log("before set", projectCopy.issues)

          setProject(projectCopy);
          // delete project.issues[source['index']]

          // console.log("Updated", project)
          // setLocal(project)
          
        });
        
        toast.success('Issue has been successfully updated.');
      } catch (error) {
        console.log(error)
        // Form.handleAPIError(error, form);
      }
    
    
    // api.optimisticUpdate(`/issues/${issueId}`, {
    //   updatedFields: {
    //     status: destination.droppableId,
    //     listPosition: calculateIssueListPosition(project.issues, destination, source, issueId),
    //   },
    //   currentFields: project.issues.find(({ id }) => id === issueId),
    //   setLocalData: fields => updateLocalProjectIssues(issueId, fields),
    // });
  };

  return (
    <DragDropContext onDragEnd={handleIssueDrop}>
      <Lists>
        {Object.values(IssueStatus).map(status => (
          <List
            key={status}
            status={status}
            project={project}
            filters={filters}
            currentUserId={currentUserId}
          />
        ))}
      </Lists>
    </DragDropContext>
  );
};

const isPositionChanged = (destination, source) => {
  if (!destination) return false;
  const isSameList = destination.droppableId === source.droppableId;
  const isSamePosition = destination.index === source.index;
  return !isSameList || !isSamePosition;
};

const calculateIssueListPosition = (...args) => {
  const { prevIssue, nextIssue } = getAfterDropPrevNextIssue(...args);
  let position;

  if (!prevIssue && !nextIssue) {
    position = 1;
  } else if (!prevIssue) {
    position = nextIssue.listPosition - 1;
  } else if (!nextIssue) {
    position = prevIssue.listPosition + 1;
  } else {
    position = prevIssue.listPosition + (nextIssue.listPosition - prevIssue.listPosition) / 2;
  }
  return position;
};

const getAfterDropPrevNextIssue = (allIssues, destination, source, droppedIssueId) => {
  const beforeDropDestinationIssues = getSortedListIssues(allIssues, destination.droppableId);
  const droppedIssue = allIssues.find(issue => issue.id === droppedIssueId);
  const isSameList = destination.droppableId === source.droppableId;

  const afterDropDestinationIssues = isSameList
    ? moveItemWithinArray(beforeDropDestinationIssues, droppedIssue, destination.index)
    : insertItemIntoArray(beforeDropDestinationIssues, droppedIssue, destination.index);

  return {
    prevIssue: afterDropDestinationIssues[destination.index - 1],
    nextIssue: afterDropDestinationIssues[destination.index + 1],
  };
};

const getSortedListIssues = (issues, status) =>
  issues.filter(issue => issue.status === status).sort((a, b) => a.listPosition - b.listPosition);

ProjectBoardLists.propTypes = propTypes;

export default ProjectBoardLists;