import React, {useState, Fragment } from 'react';
import PropTypes from 'prop-types';

import api from 'shared/utils/api';
import useApi from 'shared/hooks/api';
import toast from 'shared/utils/toast';
import { PageError, CopyLinkButton, Button, AboutTooltip } from 'shared/components';

import Loader from './Loader';
import Type from './Type';
import Delete from './Delete';
import Title from './Title';
import Description from './Description';
import Comments from './Comments';
import Status from './Status';
import AssigneesReporter from './AssigneesReporter';
import Priority from './Priority';
import EstimateTracking from './EstimateTracking';
import Dates from './Dates';
import { TopActions, TopActionsRight, Content, Left, Right } from './Styles';

const propTypes = {
  issueId: PropTypes.string.isRequired,
  projectUsers: PropTypes.array.isRequired,
  fetchProject: PropTypes.func.isRequired,
  updateLocalProjectIssues: PropTypes.func.isRequired,
  modalClose: PropTypes.func.isRequired,
};
const axios = require('axios').default;

const ProjectBoardIssueDetails = ({
  issueId,
  projectUsers,
  fetchProject,
  updateLocalProjectIssues,
  modalClose,
}) => {
  const [issue, setData] = useState(null);

  !issue && axios.get(`http://localhost:2020/issues/${issueId}`)
  .then(function (response) {
    // handle success
    console.log('In Issue details',response.data);
    setData(response.data);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  });
  const [{ proj, error, setLocalData }, fetchIssue] = useApi.get(`http://localhost:2020/issues/${issueId}`);

  // if (!data) return <Loader />;
  // if (error) return <PageError />;


  const updateLocalIssueDetails = fields =>
    setLocalData(currentData => ({ issue: { ...currentData.issue, ...fields } }));

  const updateIssue = async updatedFields => {
    console.log('I am here', updatedFields)
    try {
      await axios.post(`http://localhost:2020/issues/updateDetails/${issueId}`, updatedFields).then(() => {
        // setProject();
        // console.log("project", project.issues[source['index']].status, " dest id", destination.droppableId)
        // const projectCopy = structuredClone(project)
        console.log("fields",issue)
        const newIssue = {...issue, ...updatedFields}
        setData(newIssue)
        console.log("updated ", newIssue)
        updateLocalProjectIssues(newIssue)
        // updateLocalIssueDetails()
        // var currentFields = issue
        // var setLocalData = fields => {
        //   updateLocalIssueDetails(fields);
        //   updateLocalProjectIssues(issue.id, fields);
        // }
        // const updatedProjectIssues = project.issues.map( (item) => {
        //   if (item.id === issueId && item.status === source.droppableId){
        //     console.log(item.title, 'changing this')
        //     item.status = destination.droppableId
        //     item.listPosition = calculateIssueListPosition(project.issues, destination, source, issueId)
        //   }
        //   return item
        // });
        // projectCopy.issues = updatedProjectIssues;
        // console.log(projectCopy, 'updated issues', updatedProjectIssues);
        // // projectCopy.issues[projectIndex].status = destination.droppableId
        // // console.log("after change", projectCopy.issues[projectIndex])

        
        // // project.issues[destination.draggableId] = project.issues[source['index']]
        // console.log("before set", projectCopy.issues)

        // setProject(projectCopy);
        // delete project.issues[source['index']]

        // console.log("Updated", project)
        // setLocal(project)
        
      });
      
      toast.success('Issue has been successfully updated.');
    } catch (error) {
      console.log(error)
      // Form.handleAPIError(error, form);
    }
    // api.optimisticUpdate(`http://localhost:2020/issues/update/${issueId}`, {
    //   updatedFields,
    //   currentFields: issue,
    //   setLocalData: fields => {
    //     updateLocalIssueDetails(fields);
    //     updateLocalProjectIssues(issue.id, fields);
    //   },
    // });

  };
  if(!issue){
    return 'loading...'
  }
  return (
    <Fragment>
      <TopActions>
        <Type issue={issue} updateIssue={updateIssue} />
        <TopActionsRight>
          <AboutTooltip
            renderLink={linkProps => (
              <Button icon="feedback" variant="empty" {...linkProps}>
                Give feedback
              </Button>
            )}
          />
          <CopyLinkButton variant="empty" />
          <Delete issue={issue} fetchProject={fetchProject} modalClose={modalClose} />
          <Button icon="close" iconSize={24} variant="empty" onClick={modalClose} />
        </TopActionsRight>
      </TopActions>
      <Content>
        <Left>
          <Title issue={issue} updateIssue={updateIssue} />
          <Description issue={issue} updateIssue={updateIssue} />
          <Comments issue={issue} fetchIssue={fetchIssue} />
        </Left>
        <Right>
          <Status issue={issue} updateIssue={updateIssue} />
          <AssigneesReporter issue={issue} updateIssue={updateIssue} projectUsers={projectUsers} />
          <Priority issue={issue} updateIssue={updateIssue} />
          <EstimateTracking issue={issue} updateIssue={updateIssue} />
          <Dates issue={issue} />
        </Right>
      </Content>
    </Fragment>
  );
};

ProjectBoardIssueDetails.propTypes = propTypes;

export default ProjectBoardIssueDetails;
