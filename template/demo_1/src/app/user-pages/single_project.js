import React, { Component, useState, useEffect } from 'react';
import { Doughnut } from 'react-chartjs-2';
import Slider from "react-slick";
import { Form } from 'react-bootstrap';
import { TodoListComponent } from '../apps/TodoList'
import { VectorMap } from "react-jvectormap"
import YoutubeEmbed from "./you-tube";
import { CCardImage } from '@coreui/react';

import { Link, useLocation} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import { CardActionArea, Grid, Card, CardMedia, CardContent, Typography } from '@mui/material';

const useStyles = makeStyles(theme => ({
  container:{
      justify: 'center',
      
  },
  card: {
      border: '1px solid red',
      background: 'transparent',
      height: '100%',
      width: '95%'
  },
  
  cardActions:{
      height: '100%',
      align: 'center',
      justify: 'center'
      
  },
  icongrid:{
      margin: '5px 1rem',
  },
  icon:{
      width: theme.spacing(5),
      height: theme.spacing(5),
  },
  container:{

  },
  title: {
      textAlign: 'center',
      fontSize: '2em',
      paddingBottom: '1em',

  }
}))

const Single_Project = () => {
  const classes = useStyles();
  const location = useLocation()

  // useEffect(() => {
  //   console.log("location", location)
  // }, [location])

  const curData = location.state.dataItem
  
  return(
    <div className={classes.container} id='single_project'>
      <Typography className={classes.title} variant='h2'>
          {curData.title}
      </Typography>
      {curData.video && <Grid container>
        <Grid item style={{width:'100%'}}><Typography variant='subtitle1'>Pitch Video</Typography></Grid>
        <Grid item style={{width:'100%'}}><YoutubeEmbed embedId={curData.video}/></Grid>
      </Grid>
      }
      
      {/* <Grid container spacing={6} className={classes.outergrid} >
          
          {projectData.map(data => (
              <Grid item sm={12} lg={6} xl={4} className={classes.container} key={data.title}>
                  <ProjectCard dataItem={data} />
              </Grid>
          ))}
          
      </Grid> */}
    </div>
  )
}

export default Single_Project

// export class Single_Project extends Component {


//   render () {
//     return (
//       <div class="video">
//         <h2>Student Helper</h2>
//         <h4>DOMAIN: Software Development</h4>
//         <div class="row">
//         <div className="col-6 grid-margin stretch-card">
//             To get a aggregated inventory of all previous keystone and capstone projects for students to lookup as well as for faculty to check for plagiarism
//             Easy collaboration between student team and faculty
//             Easy transition and collaboration between the keystone and capstone project teams
//             Helpful in planning and scheduling courses for the upcoming quarter
//         </div>
//         <div className="col-6 grid-margin stretch-card">
//             <img src={require("../../assets/images/GitHub-Mark.png")} alt="logo" width="170" height="170"/>
//         </div>
//         </div>
//         <YoutubeEmbed embedId="rokGy0huYEA" />
//       </div>
//     );
//   }
// }

// export default Single_Project;
