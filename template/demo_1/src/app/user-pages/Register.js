import React, { Component, useState } from 'react';
import { Link } from 'react-router-dom';

//export class Register extends Component {
export default function Register(){
  const[userId, setUser]=useState('');
  const[firstName, setName]=useState('');
  const[lastName, setLast]=useState('');
  const[emailId, setEmail]=useState('');
  const[phone, setPhone]=useState('');
  const[role, setRole]=useState('Student');
  const[password, setPass]=useState('');
  
  const handleClick=(e)=>{
    const newUser={userId,firstName,lastName,emailId,phone,role,password};
    fetch("http://localhost:2020/users/add",{
      method:"POST",
      headers:{"Content-Type": "application/json"},
      body:JSON.stringify(newUser)})
    .then(()=>{
        console.log("New user added.")
      })
    .catch(err => {
      console.log(err.message);
      console.log('Back-end is offline.')
    })
  }
  //render() {
    return (
      <div>
        <div className="d-flex align-items-center auth px-0 h-100">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="card text-left py-5 px-4 px-sm-5">
                <div className="brand-logo">
                  <center><img src={require("../../assets/images/logo2.png")} alt="logo" /></center>
                </div>
                <h4>New here?</h4>
                <h6 className="font-weight-light">Signing up is easy. It only takes a few steps</h6>
                <form className="pt-3">
                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" id="exampleInputUsername1" placeholder="Username" 
                    value={userId}
                    onChange={(e)=>setUser(e.target.value)}/>
                  </div>
                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" id="exampleInputFirstname1" placeholder="First Name" 
                    value={firstName}
                    onChange={(e)=>setName(e.target.value)}/>
                  </div>
                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" id="exampleInputLastname1" placeholder="Last Name" 
                    value={lastName}
                    onChange={(e)=>setLast(e.target.value)}/>
                  </div>
                  <div className="form-group">
                    <input type="email" className="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" 
                    value={emailId}
                    onChange={(e)=>setEmail(e.target.value)}/>
                  </div>
                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" id="exampleInputPhone1" placeholder="Phone Number" 
                    value={phone}
                    onChange={(e)=>setPhone(e.target.value)}/>
                  </div>
                  <div className="form-group">
                    <select className="form-control form-control-lg" id="exampleFormControlSelect2"
                    value={role}
                    onChange={(e)=>setRole(e.target.value)}>
                        <option>Student</option>
                        <option>Professor</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <input type="password" className="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" 
                    value={password}
                    onChange={(e)=>setPass(e.target.value)}/>
                  </div>
                  <div className="mb-4">
                    <div className="form-check">
                      <label className="form-check-label text-muted">
                        <input type="checkbox" className="form-check-input" />
                        <i className="input-helper"></i>
                        I agree to all Terms & Conditions
                      </label>
                    </div>
                  </div>
                  <div className="mt-3">
                    <Link to="/dashboard">
                      <button className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="button"
                      onClick={handleClick}>
                        SIGN UP
                      </button> 
                    </Link>
                  </div>
                  <div className="text-center mt-4 font-weight-light">
                    Already have an account? <Link to="/user-pages/login-1" className="text-primary">Login</Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
 // }
}

//export default Register
