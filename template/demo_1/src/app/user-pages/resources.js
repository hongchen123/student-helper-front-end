import React, { Component, useState, useEffect } from 'react';
import { Doughnut } from 'react-chartjs-2';
import Slider from "react-slick";
import { Form } from 'react-bootstrap';
import { TodoListComponent } from '../apps/TodoList'
import { VectorMap } from "react-jvectormap"

import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';


export default function Dashboard() {
  const mapData = {
    "BZ": 75.00,
    "US": 56.25,
    "AU": 15.45,
    "GB": 25.00,
    "RO": 10.25,
    "GE": 33.25
  }
  const[users, setUsers]=useState([])
  const paperStyle={width:200, margin:"20px auto"}

  useEffect(()=>{
    fetch("http://localhost:2020/users")
    .then((res) => {
      if (res.ok){
        return res.json();
      }
      throw new Error('Failed to fetch data.');
    })
    .then((result)=>{
      setUsers(result);
    })
    .catch((error) => {
      console.log(error.message);
      console.log('Back-end is offline');
      // throw new Error('Back-end is offline.');
    })
  }, [users]);

    return (
      <Box sx={{ flexGrow: 1 }}>

        <Paper elevation={3} style={paperStyle}>
          <Typography variant="h6" color="inherit" component="div" style={{textAlign:"center"}}>
            Users
          </Typography>
        </Paper>

        <Grid container spacing={2}>
          {users.map(user=>(
            <Grid item xs={12} md={6} lg={3}>
              <Paper elevation={6} style={{margin:"10px", padding:"15px",textAlign:"center"}}>
                ID:  {user.userId}<br/><br/>
                Name:  {user.firstName + '  ' + user.lastName}<br/><br/>
                Email:  {user.emailId}<br/><br/>
                Phone Number:  {user.phone}<br/><br/>
              </Paper>
            </Grid>
          ))}
           
        </Grid>
    </Box>

    );
  
}

