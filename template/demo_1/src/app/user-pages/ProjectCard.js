import React from 'react';
//import { makeStyles } from '@mui/styles';
import {makeStyles} from '@material-ui/core/styles';
import { CardActionArea, Grid, Card, CardMedia, CardContent, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    container:{
        justify: 'center',
        
    },
    card: {
        border: '1px solid red',
        background: 'transparent',
        height: '100%',
        width: '95%'
    },
    
    cardActions:{
        height: '100%',
        align: 'center',
        justify: 'center'
        
    },
    icongrid:{
        margin: '5px 1rem',
    },
    icon:{
        width: theme.spacing(5),
        height: theme.spacing(5),
    }
}))

const ProjectCard = ({dataItem}) => {
    const classes = useStyles();

    return(
        
            <Card className={classes.card}>
                <CardActionArea >
                    <Link to={{
                        pathname: "/user-pages/single-project",
                        state: {dataItem}
                    }}>
                        <CardMedia component='img'
                            alt={dataItem.imgAlt}
                            height='240'
                            image={dataItem.image}
                            title={dataItem.imgAlt}
                            />
                    </Link>   
                </CardActionArea>
                <CardContent >
                    <Typography gutterBottom variant="h4" component="h2" style={{textAlign:'center'}}>
                        {dataItem.title}
                    </Typography>
                    {dataItem.description.map(item => (
                        <Typography key={item} variant="subtitle1" color="textSecondary" component="p" style={{fontSize:'1.2em', justify:'center', paddingTop:'15px'}}>
                            {item}
                        </Typography>
                    ))}
                </CardContent>
            </Card>
        


    )
}

export default ProjectCard