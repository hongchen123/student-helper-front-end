import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import Slider from "react-slick";
import { Form } from 'react-bootstrap';
import { TodoListComponent } from '../apps/TodoList'
import { VectorMap } from "react-jvectormap"
import {CCard} from '@coreui/react';
import {CCardBody} from '@coreui/react';
import { CCardImage } from '@coreui/react';
import { CCardTitle } from '@coreui/react';
import { CCardText } from '@coreui/react';
import { CButton } from '@coreui/react'

import {makeStyles} from '@material-ui/core/styles';
//import { makeStyles } from '@material-ui/styles';
import {Grid, Typography} from '@mui/material';
import ProjectCard from './ProjectCard';
import projectData from './ProjectData';


const useStyles = makeStyles(theme => ({
  container:{

  },
  title: {
      textAlign: 'center',
      fontSize: '2em',
      paddingBottom: '1em',

  },
  outergrid:{
      width: '100%',
      justifyContent: 'center',
      margin: 'auto',
  }
}))

const All_Project = () => {
  const classes = useStyles()

  return(
      projectData.length > 0 && (
          <div className={classes.container} id='projects'>
              <Typography className={classes.title} variant='h2'>
                  Capstone Projects
              </Typography>

              <Grid container spacing={6} className={classes.outergrid} >
                  
                  {projectData.map(data => (
                      <Grid item sm={12} lg={6} xl={4} className={classes.container} key={data.title}>
                          <ProjectCard dataItem={data} />
                      </Grid>
                  ))}
                  
              </Grid>
          </div>
          
      )
      
  )
}


// export class All_Project extends Component {


//   render () {
//     return (
//       <div class="row">
//         <div className="col-6 grid-margin stretch-card">
//           <CCard style={{ width: '18rem' }}>
//           <CCardBody>
//             <CCardTitle>Project 1</CCardTitle>
//             <CCardText>
//             Project 1 description
//             </CCardText>
//             <CButton href="/user-pages/single-project">Link to project</CButton>
//           </CCardBody>
//           </CCard>
//         </div>
//         <div className="col-6 grid-margin stretch-card">
//           <CCard style={{ width: '22rem' }}>
//           <CCardBody>
//             <CCardTitle>Project 2</CCardTitle>
//             <CCardText>
//             Project 2 description
//             </CCardText>
//             <CButton href="/user-pages/single-project">Link to project</CButton>
//           </CCardBody>
//           </CCard>
//         </div>
//       </div>
//     );
//   }
// }

export default All_Project;
